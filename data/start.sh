#!/bin/bash

qmlscene $@ active_screen.qml &

mkdir -p $HOME/.cache/ut-sysbench-qt-gui/sh
ln -sf $(pwd)/sh/bench_cpu.sh $HOME/.cache/ut-sysbench-qt-gui/sh/bench_cpu.sh
ln -sf $(pwd)/sh/bench_multiple.sh $HOME/.cache/ut-sysbench-qt-gui/sh/bench_multiple.sh
ln -sf $(pwd)/sh/start_test.sh $HOME/.cache/ut-sysbench-qt-gui/sh/start_test.sh
ln -sf $(pwd)/sh/testing.sh $HOME/.cache/ut-sysbench-qt-gui/sh/testing.sh
ln -sf $(pwd)/sh/compare.sh $HOME/.cache/ut-sysbench-qt-gui/sh/compare.sh
ln -sf $(pwd)/background_image.png $HOME/.cache/ut-sysbench-qt-gui/background_image.png
ln -sf $(pwd)/ut-sysbench-qt-gui.qml $HOME/.cache/ut-sysbench-qt-gui/ut-sysbench-qt-gui.qml

mkdir -p $HOME/.cache/ut-sysbench-qt-gui/messages
cp -R messages/* $HOME/.cache/ut-sysbench-qt-gui/messages/

gui_version=$(pwd | grep -o '[^/]\+$')
sysbench_version=$(sysbench version | grep sysbench)
framework=$(<messages/framework.txt)
license=$(<messages/LICENSE_info)
info_txt=$'INFO\n--------------------\n\n'
info_txt="${info_txt}SOFTWARE VERSIONS:"$'\n\n'
info_txt="${info_txt}ut-sysbench-qt-gui version ${gui_version}"$'\n\n'
info_txt="${info_txt}${sysbench_version}"$'\n\n'
info_txt="${info_txt}${framework}"$'\n\n\n'
info_txt="${info_txt}LICENSE:"$'\n\n'"${license}"
echo "${info_txt}" > $HOME/.cache/ut-sysbench-qt-gui/messages/info.txt

cd $HOME/.cache/ut-sysbench-qt-gui
cp messages/start.txt messages/temp_message.txt

chmod a+rwx -R *

rm -R sh/testing
rm -R sh/compare

ut-sysbench-qt-gui

exit
