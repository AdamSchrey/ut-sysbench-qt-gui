#pragma once
#include <QTimer>
#include <QObject>
#include <QString>
#include <QGuiApplication>

class ut_sysbench_qt_gui : public QObject {

	Q_OBJECT
	Q_PROPERTY(QString info READ info)
	Q_PROPERTY(QString display READ display NOTIFY displayChanged)
	Q_PROPERTY(bool interactive READ interactive NOTIFY interactiveChanged)

	public:
		ut_sysbench_qt_gui(QObject *parent = nullptr);
		QString info();
		QString display();
		bool interactive();

	signals:
		void displayChanged();
		void interactiveChanged();
	
	public slots:
		void clear();
		void compare();
		void bench_cpu();
		void bench_multiple();

	private:
		QString m_display;
		bool m_interactive;

  	private slots:
		void update_lab();
		void please_wait();

	private:
		QTimer *timer;
};

