#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

//modified functions to prevent mistakes when dealing with strings:
//sizeof is not able to get the size of string passed as parameter => third parameter size is needed in strncpy_mod and strncat_mod

static int strcmp_mod(const void* a, const void* b) 
{
	//compare first the length of the strings and compare the characters later
	if (strlen(*(const char**)a) < strlen(*(const char**)b)){return 0;}
	if (strlen(*(const char**)b) < strlen(*(const char**)a)){return 1;}
    return strcmp(*(const char**)a, *(const char**)b); 
}

void strncpy_mod (char *restrict dest, const char *restrict source, const int size)
{
	strncpy (dest, source, size - 1);
	dest[size - 1] = '\0';
}

void strncat_mod (char *restrict dest, const char *restrict source, const int size)
{
	strncat(dest, source, size - strlen(dest) - 1);
}

void get_text(const char *path_to_text, char *text, const int size){
	FILE *fp = NULL;
	char *line = NULL;
	size_t line_buf_size = 0;
	int line_count = 0;
	ssize_t line_size;

	fp = fopen(path_to_text, "r");
	if (!fp) {
		perror ("File open error!\n");
	}

	line_size = getline(&line, &line_buf_size, fp);

	while (line_size >= 0){
		line_count++;
		if (strlen(line) > 1000){
			line[999]  = '\n';
			line[1000] = '\0';
		}		
		strncat_mod(text,line,size*sizeof(char));
		line_size = getline(&line, &line_buf_size, fp);
	}

	free(line);
	line = NULL;
	fclose (fp);
}