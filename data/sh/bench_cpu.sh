#!/bin/bash

echo -e '\nsysbench --test=cpu --num-threads=2 --cpu-max-prime=20000 run' >> messages/temp_message.txt
cpu_events_per_second=$(sysbench --test=cpu --cpu-max-prime=20000 --num-threads=2 run | grep -oP '(?<=events per second: )(.*)(?=\.)')
echo "$cpu_events_per_second" > sh/cpu_events_per_second.txt
echo "    $cpu_events_per_second events per second" >> messages/temp_message.txt

rm -R sh/testing/
rm -R sh/compare/

exit
