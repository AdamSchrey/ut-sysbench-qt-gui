#include "ut-sysbench-qt-gui.h"
#include <QClipboard>
#include <QProcess>
#include <QString>

//C code for reading from a text file
//(can also be used in a later C and GTK version of this program,
//therefore no C++ code):
#include "text_io.h"


ut_sysbench_qt_gui::ut_sysbench_qt_gui(QObject *parent): QObject(parent) {

	timer = new QTimer(this);
	//timer->setSingleShot(false);

	char start_text[10000]="";
	get_text("messages/temp_message.txt", start_text, 10000);

	m_display = QString(start_text);
	m_interactive = true;

	connect(timer, &QTimer::timeout, this, &ut_sysbench_qt_gui::update_lab);
	timer->start(500);
}

QString ut_sysbench_qt_gui::info(){
	char info_txt[32500]="";
	get_text("messages/info.txt", info_txt, 32500);
	return info_txt;
}

QString ut_sysbench_qt_gui::display(){
	return m_display;
}

bool ut_sysbench_qt_gui::interactive(){
	return m_interactive;
}

void ut_sysbench_qt_gui::update_lab(){
	if (QProcess::execute("sh/testing.sh") == true){

		int num_end = m_display[m_display.size()-1].unicode();
		if (num_end >= 9641){
			m_display[m_display.size()-1] = QChar(8709);
		} else if (num_end < 8709){
			m_display[m_display.size()-1] = QChar(8709);
		} else {
			m_display[m_display.size()-1] = QChar(num_end+1);
		}

		if (m_display[m_display.size()-3] == QChar(9716)){
			m_display[m_display.size()-3] = QChar(9719);
		} else if (m_display[m_display.size()-3] == QChar(9719)){
			m_display[m_display.size()-3] = QChar(9718);
		} else if (m_display[m_display.size()-3] == QChar(9718)){
			m_display[m_display.size()-3] = QChar(9717);
		} else {
			m_display[m_display.size()-3] = QChar(9716);
		}
		emit displayChanged();
	}else{
		char new_text[16000]="";
		get_text("messages/temp_message.txt", new_text, 16000);
		m_display = QString(new_text);
		emit displayChanged();
		m_interactive = true;
		emit interactiveChanged();
	}
	
}

void ut_sysbench_qt_gui::please_wait(){	
	char new_text[10000]="";
	get_text("messages/wait.txt", new_text, 10000);
	m_display = QString(new_text);
	emit displayChanged();
	m_interactive = false;
	emit interactiveChanged();
}

void ut_sysbench_qt_gui::bench_multiple(){
	static char script[] = "./sh/start_test.sh multiple";	
	QProcess::execute(QString(script));
	ut_sysbench_qt_gui::please_wait();
}

void ut_sysbench_qt_gui::bench_cpu(){	
	static char script[] = "./sh/start_test.sh cpu";	
	QProcess::execute(QString(script));
	ut_sysbench_qt_gui::please_wait();
}

void ut_sysbench_qt_gui::clear(){	
	static char script[] = "cp messages/empty.txt messages/temp_message.txt";
	QProcess::execute(QString(script));
	ut_sysbench_qt_gui::update_lab();
}

void ut_sysbench_qt_gui::compare(){	
	if (QProcess::execute("sh/compare.sh") == true){
		QClipboard *clip = QGuiApplication::clipboard();
		clip->setText(m_display);
		ut_sysbench_qt_gui::update_lab();
	}else{
		ut_sysbench_qt_gui::update_lab();
	}
}