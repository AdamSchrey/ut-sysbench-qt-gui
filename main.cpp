#include "ut-sysbench-qt-gui.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QQmlContext>

int main(int argc, char *argv[]) {
	QGuiApplication app(argc, argv);
	QQmlApplicationEngine engine;
	engine.load(QUrl("ut-sysbench-qt-gui.qml"));

	ut_sysbench_qt_gui data;
	QQmlContext *rootContext = engine.rootContext();
	rootContext->setContextProperty("cppObject", &data);

	QObject *topLevel = engine.rootObjects().value(0);
	QQuickWindow *window = qobject_cast<QQuickWindow *>(topLevel);
	window->show();
	return app.exec();
}