import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Lomiri.Components 1.3
import Qt.labs.settings 1.0


ApplicationWindow {
	width: 310
	height: 600
	title: "ut-sysbench-qt-gui"
	color: theme.palette.normal.background
	
	Dialog {
		ScrollView {
			Text {
				font.family: "Ubuntu Mono"
				text: cppObject.info
				wrapMode: Text.WordWrap
				color: theme.palette.normal.foregroundText
				width: parent.parent.width
			}
		height: parent.height
		width: parent.width
		}
		width: 0.9 * parent.width
		height: 0.7 * parent.height
		font.family: "Ubuntu Mono"
		id: info_dialog
		modal: true
		x: (parent.width - width) / 2
		y: (parent.height - height) / 8
		standardButtons: Dialog.Ok
	}
	GridLayout {
		id: grid
		anchors.fill: parent
		rows: 7
		columns: 2
		Rectangle {
			color: theme.palette.normal.base
			height: 2
			Layout.fillWidth: true
			Layout.columnSpan: 2
			Layout.rowSpan: 1
		}
		RowLayout {
			Layout.columnSpan: 2
			Text {
				id: titel
				text: "ut-sysbench-qt-gui"
				font.family: "Ubuntu Mono"
				font.pointSize: 40
				font.bold: true 
				color: theme.palette.normal.baseText
				Layout.fillWidth: true
				Layout.leftMargin: 25
				Layout.rightMargin: 25
			}
			Button{
				Layout.bottomMargin: 10
				Layout.topMargin: 5
				Layout.rightMargin: 5
				font.family: "Ubuntu Mono"
				font.pointSize: 40
				font.bold: true 
				text: "i"
				onClicked: info_dialog.open()
				enabled: cppObject.interactive
				color: theme.palette.normal.foreground
			}
		}
		Rectangle {
			color: theme.palette.normal.base
			height: 2
			Layout.fillWidth: true
			Layout.columnSpan: 2
			Layout.rowSpan: 1
		}
		ScrollView {
			id: scroll			
			Image {
				source: "background_image.png"
				anchors.right: parent.right
				anchors.bottom: parent.bottom
				z: -1
			}
			Text {
				width: scroll.width
				wrapMode: Text.WordWrap
				font.family: "Ubuntu Mono"
				text: cppObject.display
				color: theme.palette.normal.baseText
			}
			Layout.topMargin: 5
			Layout.leftMargin: 25
			Layout.rightMargin: 25
			Layout.fillWidth: true
			Layout.fillHeight: true
			Layout.columnSpan: 2
			Layout.rowSpan: 1
			Layout.alignment: Qt.AlignCenter
		}
		Rectangle {
			color: theme.palette.normal.base
			height: 2
			Layout.fillWidth: true
			Layout.columnSpan: 2
			Layout.rowSpan: 1
		}
		Button {
			font.family: "Ubuntu Mono"
			Layout.bottomMargin: 5
			Layout.topMargin: 5
			Layout.leftMargin: 5
			Layout.fillWidth: true
			Layout.columnSpan: 1
			text: "Clear Text"
			onClicked: {cppObject.clear(); titel.text = "ut-sysbench-qt-gui"}
			enabled: cppObject.interactive
			color: theme.palette.normal.foreground
                }
                Button {
			font.family: "Ubuntu Mono"
			Layout.bottomMargin: 10
			Layout.topMargin: 5
			Layout.rightMargin: 5
			Layout.fillWidth: true
			Layout.columnSpan: 1
			text: "Export and Compare"
			onClicked: {cppObject.compare(); titel.text = "Export and Compare"}
			enabled: cppObject.interactive
			color: theme.palette.normal.foreground
                }
		Button {
			font.family: "Ubuntu Mono"
			Layout.bottomMargin: 10
			Layout.leftMargin: 5
			Layout.fillWidth: true
			Layout.columnSpan: 1
			text: "Multiple Benchmarks"
			onClicked: {cppObject.bench_multiple(); titel.text = "Sysbench Results"}
			enabled: cppObject.interactive
			color: theme.palette.normal.focus
		}
		Button {
			font.family: "Ubuntu Mono"
			Layout.bottomMargin: 5
			Layout.rightMargin: 5
			Layout.fillWidth: true
			Layout.columnSpan: 1
			text: "CPU Benchmark"
			onClicked: {cppObject.bench_cpu(); titel.text = "Sysbench Results"}
			enabled: cppObject.interactive
			color: theme.palette.normal.focus
		}
	}
}
