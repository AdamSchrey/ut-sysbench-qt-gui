#!/bin/bash

set -Eeou pipefail

echo "ARCH: ${ARCH}"

apt update
apt -y upgrade

cd "${ROOT}"

echo -e "\n\n${SDK_FRAMEWORK} > data/messages/framework.txt\n"
echo ${SDK_FRAMEWORK} > data/messages/framework.txt

echo -e "\n\nImport Libraries:\n"

mkdir -p lib/${ARCH_TRIPLET}/

apt download libaio1:${ARCH}
libaio=$(ls | grep "libaio")
dpkg-deb -xv $libaio .
cp -r usr/lib/* lib/
rm libaio*
rm -R usr

apt download libc6:${ARCH}
libc=$(ls | grep "libc")
dpkg-deb -xv $libc .
cp -r usr/lib/* lib/
rm libc*
rm -R usr

apt download libluajit-5.1-2:${ARCH}
libluaji=$(ls | grep "libluaji")
dpkg-deb -xv $libluaji .
cp -r usr/lib/* lib/
rm liblua*
rm -R usr

apt download libmysqlclient21:${ARCH}
libmysqlclient=$(ls | grep "libmysqlclient")
dpkg-deb -xv $libmysqlclient .
cp -r usr/lib/* lib/
rm libmysqlclient*
rm -R usr

apt download libpq5:${ARCH}
libpq=$(ls | grep "libpq")
dpkg-deb -xv $libpq .
cp -r usr/lib/* lib/
rm libpq*
rm -R usr

apt download libpcre3:${ARCH}
libpcre=$(ls | grep "libpcre")
dpkg-deb -xv $libpcre .
cp -r usr/lib/* lib/
rm libpcre*
rm -R usr

echo -e "\n\nImport Programs:\n"

apt download coreutils:${ARCH}
coreutils=$(ls | grep "coreutils")
echo $coreutils
dpkg-deb -xv $coreutils .
cp bin/cp data/
cp bin/ln data/
cp bin/mkdir data/
cp bin/mv data/
cp bin/echo data/
cp bin/rm data/
rm coreutils_*
rm -R usr
rm -R bin

apt download diffutils:${ARCH}
diffutils=$(ls | grep "diffutils")
echo $diffutils
dpkg-deb -xv $diffutils .
cp usr/bin/cmp data/
rm diffutils_*
rm -R usr

apt download bc:${ARCH}
bc_p=$(ls | grep "bc_1")
echo $bc_p
dpkg-deb -xv $bc_p .
cp usr/bin/bc data/
rm bc_*
rm -R usr

apt download grep:${ARCH}
grep_p=$(ls | grep "grep")
echo $grep_p
dpkg-deb -xv $grep_p .
cp bin/grep data/
rm grep_*
rm -R usr
rm -R bin

if [[ ${ARCH} == "armhf" ]]
then
	#could not download armhf package from official Ubuntu repos for some reason
	#use Debian repos instead
	wget http://ftp.debian.org/debian/pool/main/s/sysbench/sysbench_1.0.20+ds-1_armhf.deb
else
	apt download sysbench:${ARCH}
fi
	sysbench=$(ls | grep "sysbench_1")
	echo $sysbench
	dpkg-deb -xv $sysbench .
	cp usr/bin/sysbench data/
	rm sysbench_1*
	rm -R usr

exit
