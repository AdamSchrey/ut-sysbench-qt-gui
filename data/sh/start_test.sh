#!/bin/bash

mkdir sh/testing/

if	 cmp -s "messages/temp_message.txt" "messages/start.txt" 	|| \
	 cmp -s "messages/temp_message.txt" "messages/empty.txt" 	|| \
	 cmp -s "messages/temp_message.txt" "messages/compare.txt" 	|| \
	[ -d "sh/compare" ]; then
    cp messages/framework.txt messages/temp_message.txt
fi

case "$1"  in
	"cpu") ./sh/bench_cpu.sh & ;;
	"multiple")  ./sh/bench_multiple.sh & ;;

esac

exit