#!/bin/bash
if	cmp -s "messages/temp_message.txt" "messages/start.txt" 	|| \
	cmp -s "messages/temp_message.txt" "messages/empty.txt" ; then
	cp messages/compare.txt messages/temp_message.txt
    exit 0
fi

if cmp -s "messages/temp_message.txt" "messages/compare.txt" ; then
    exit 0
fi

if [ -d "sh/compare" ]; then
	exit 0
fi

mkdir sh/compare/

create_diagram () {
	last_i=$((${#device_list[@]} - 1))
	
	max_perf=${performance_list[0]}
	if (( ${performance_list[0]} < $this_device_performance )) ; then
		max_perf=$this_device_performance
	fi
	
	min_perf=${performance_list[$last_i]}
	if (( ${performance_list[${last_i}]} > $this_device_performance )) ; then
		min_perf=$this_device_performance
	fi
	
	print_this=1
	for (( i=0; i<$(( $last_i + 1 )); i++ )) ; do
		if (( (( $(echo "$this_device_performance > ${performance_list[$i]}" | bc -l) )) && $print_this )) ; then
			message=$"${message}"$'\n'"This device | ${this_device_performance} $1"$'\n'"..."
			number_blocks=$(bc <<< "scale=6; 24*(($this_device_performance - $min_perf)/($max_perf-$min_perf)) + 1")
			number_blocks=$(echo "$number_blocks/1" | bc)
			for (( j=0; j<$number_blocks; j++ )) ; do
				message="${message}${medium_block}"
			done
			print_this=0
		fi
		echo $i
		echo $message
		message=$"${message}"$'\n'"${device_list[$i]} | ${performance_list[$i]} $1"$'\n'"..."
		number_blocks=$(bc <<< "scale=6; 24*((${performance_list[$i]} - $min_perf)/($max_perf-$min_perf)) + 1")
		number_blocks=$(echo "$number_blocks/1" | bc)
		for (( k=0; k<$number_blocks; k++ )) ; do
			message="${message}${full_block}"
		done
	done
} 

message="The results of the performed tests have been
copied to your clipboard 📋
(you can paste it into text fields
of other apps or websites)."
medium_block="▒"
full_block="█"
framework=$(<messages/framework.txt)
this_device_performance=""

message="${message}"$'\n\n'
#message=$'\033[4mUbuntu 20.04 and Sysbench 1.0.18 (1.0.20 for armhf):\033[0m \n\n'
message="${message}"$'
------------------------------------------------------------------
Ubuntu 20.04 and Sysbench 1.0.18
(Sysbench 1.0.20 for armhf
Sysbench 1.0.20 and PureOS 10 for Librem 5)
------------------------------------------------------------------\n\n'

if [[ ${framework:0:16} == "ubuntu-sdk-20.04" ]]; then
	this_device_performance=$(<sh/cpu_events_per_second.txt)
fi
device_list=("OnePlus Nord 2" "Volla X23" "Pixel 3a" "Redmi Note 7" "Gigaset GS5 Pro"
		"Xiaomi Mi A2" "Xperia X" "Poco X3 NFC" "Galaxy S10e Exynos"
		"OnePlus 6T" "Pinetab 2" "Librem 5 (PureOS)" "Pinephone")
performance_list=("2492" "1925" "1705" "1657" "1650" "1567" "1385" "1344" "1295"
			"1092" "678" "596" "453") #from best to worst
message="${message}Comparison CPU Benchmark (2 threads):"$'\n'
create_diagram "events/second"

message="${message}"$'\n\n\n'
if [[ ${framework:0:16} == "ubuntu-sdk-20.04" ]]; then
	this_device_performance=$(<sh/cpu_events_per_second2.txt)
fi
device_list=("Redmi Note 7" "OnePlus Nord 2" "Galaxy S10e Exynos" "Volla X23" "Gigaset GS5 Pro"
		"Pixel 3a" "Poco X3 NFC" "OnePlus 6T"
		"Xiaomi Mi A2" "Xperia X" "Pinetab 2" "Librem 5 (PureOS)" "Pinephone")
performance_list=("4755" "4681" "4731" "4222" "3735" "3673" "3598"
			"3063" "2890" "2465" "1263" "1191" "901") #from best to worst
message="${message}Comparison CPU Benchmark (8 threads):"$'\n'
create_diagram "events/second"

message="${message}"$'\n\n\n'
if [[ ${framework:0:16} == "ubuntu-sdk-20.04" ]]; then
	this_device_performance=$(<sh/mib_per_second.txt)
fi
device_list=("OnePlus Nord 2" "Pixel 3a" "Volla X23" "Gigaset GS5 Pro"
		"Librem 5 (PureOS)" "Galaxy S10e Exynos" "Pinetab 2" "Xiaomi Mi A2"
		"Poco X3 NFC" "Redmi Note 7" "Xperia X" "OnePlus 6T" "Pinephone")
performance_list=("12760" "6953" "6800" "5877" "5511" "5464" "4968"
			"4910" "4217" "3561" "2487" "2452" "1339") #from best to worst
message="${message}Comparison Memory Benchmark (no armhf devices):"$'\n'
create_diagram "MiB/second"

message="${message}"$'\n\n\n'
if [[ ${framework:0:16} == "ubuntu-sdk-20.04" ]]; then
	this_device_performance=$(<sh/mutex_time.txt)
fi
device_list=("Pinephone" "Librem 5 (PureOS)" "Pinetab 2" "Xperia X" "Xiaomi Mi A2"
		"OnePlus 6T" "Pixel 3a" "Gigaset GS5 Pro" "Poco X3 NFC" "Redmi Note 7"
		"Volla X23" "Galaxy S10e Exynos" "OnePlus Nord 2")
performance_list=("21.5289" "16.1842" "15.9498" "9.2790" "8.6976" "6.2309" "5.4794"
		"5.0982" "5.0033" "4.8477" "4.2907" "3.4450" "2.6850") #from worst to best
message="${message}Comparison Mutex Benchmark (less time is better):"$'\n'
create_diagram "seconds"

this_device_performance=""
message="${message}"$'\n\n'

message="${message}"$'
------------------------------------------------------------------
Ubuntu 16.04 and Sysbench 0.4.12
------------------------------------------------------------------\n\n'
if [[ ${framework:0:16} == "ubuntu-sdk-16.04" ]]; then
	this_device_performance=$(<sh/cpu_events_per_second.txt)
fi
device_list=("OnePlus Nord 2" "Xiaomi Mi A2" "Cosmo Commun." "M10 Tab + 2.gen" "Volla Phone"
			"Xperia X" "Pixel 3a" "Pinephone" "bq Aquaris M10 FHD" "Nexus 5"
			"Nexus 4" "Meizu MX4" "bq E5")
performance_list=("2392" "1717" "1652" "1235" "1091" "1035"
			"918" "620" "59" "58" "57" "52" "46" ) #from best to worst
message="${message}Comparison CPU Benchmark (2 threads):"$'\n'
create_diagram "events/second"

message="${message}"$'\n\n\n'
if [[ ${framework:0:16} == "ubuntu-sdk-16.04" ]]; then
	this_device_performance=$(<sh/cpu_events_per_second2.txt)
fi
device_list=("Cosmo Commun." "Xiaomi Mi A2" "OnePlus Nord 2" "M10 Tab + 2.gen" "Pixel 3a"
			"Volla Phone" "Xperia X" "Pinephone"
			"Meizu MX4" "bq Aquaris M10 FHD" "Nexus 4" "Nexus 5" "bq E5")
performance_list=("5287" "4915" "4593" "4005" "3973"
				"3788" "2467" "1190"
				"146" "113" "113" "96" "91") #from best to worst
message="${message}Comparison CPU Benchmark (8 threads):"$'\n'
create_diagram "events/second"

message="${message}"$'\n\n\n'
if [[ ${framework:0:16} == "ubuntu-sdk-16.04" ]]; then
	this_device_performance=$(<sh/mib_per_second.txt)
fi
device_list=("Xiaomi Mi A2" "OnePlus Nord 2" "Pixel 3a" "M10 Tab + 2.gen"
			"Cosmo Commun." "Volla Phone" "Pinephone" "Xperia X")
performance_list=("5849" "3702" "2560" "2443"
				"2300" "2250" "815" "795") #from best to worst
message="${message}Comparison Memory Benchmark (no armhf devices):"$'\n'
create_diagram "MiB/second"

message="${message}"$'\n\n\n'
if [[ ${framework:0:16} == "ubuntu-sdk-16.04" ]]; then
	this_device_performance=$(<sh/mutex_time.txt)
fi
device_list=("Meizu MX4" "Nexus 5" "Nexus 4"
			"bq E5" "Volla Phone" "M10 Tab + 2.gen" "Xperia X"
			"Cosmo Commun." "Xiaomi Mi A2" "Pinephone"
			"bq Aquaris M10 FHD" "Pixel 3a" "OnePlus Nord 2")
performance_list=("3.8107" "2.7758" "2.4609"
				"2.4057" "1.8590" "1.8361" "1.6159"
				"1.2297" "1.1919" "1.0368" "0.9229" "0.9193" "0.7016") #from worst to best
message="${message}Comparison Mutex Benchmark (less time is better):"$'\n'
create_diagram "seconds"

message="${message}"$'\n\n\n'
message="${message}The shown benchmarks only give a rough idea
about a device's performance and do not
fully represent the everyday performance of
a device."

echo "$message" > messages/temp_message.txt

exit 1
