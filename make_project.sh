#!/bin/bash

rm *.o
rm Makefile
rm *.pro
rm moc_*
rm .qmake*
rm -R .clickable build lib

qmake -project
cat add_to_pro.txt >> *.pro

qmake
rm .qmake.stash

#make

exit
