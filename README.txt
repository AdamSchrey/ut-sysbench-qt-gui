DESCRIPTION
--------------------------------------------------------------------

ut-sysbench-qt-gui is a simple GUI for sysbench. It can be used to
measure the performance of your Ubuntu Touch device.

INSTALLATION
--------------------------------------------------------------------

A precompiled version of ut-sysbench-gui can be found
on https://open-store.io/app/ut-sysbench-qt-gui.
If you decide to compile the software, you will do so at your own
risk and you will need root privileges for this.

Following programs should be installed, if you want to compile and
install ut-sysbench-qt-gui:
qt5-qmake, qtbase5-dev, qml-module-qtquick-controls,
qtdeclarative5-dev, clickable-ut, docker.io, adb, git,
python3, python3-pip, python3-setuptools
(Take a look at https://clickable-ut.dev/en/latest/install.html)

If arm64 is the architecture of your device (otherwise use
armhf or amd64) and if the framework is Ubuntu 20.04,
ut-sysbench-qt-gui can be installed on your Ubuntu Touch device
(connected to your PC and in Developer Mode) with the command
"./make_project.sh &&
CLICKABLE_FRAMEWORK=ubuntu-sdk-20.04 CLICKABLE_ARCH=arm64 clickable"
executed as root user in the directory of this project.


LICENSE
--------------------------------------------------------------------
-------------------------------------------
GNU General Public License:
-------------------------------------------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be entertain,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
