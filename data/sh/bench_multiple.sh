#!/bin/bash

echo -e '\nsysbench --test=cpu --num-threads=2 --cpu-max-prime=20000 run' >> messages/temp_message.txt
cpu_events_per_second=$(sysbench --test=cpu --cpu-max-prime=20000 --num-threads=2 run | grep -oP '(?<=events per second: )(.*)(?=\.)')
echo "$cpu_events_per_second" > sh/cpu_events_per_second.txt
echo "    $cpu_events_per_second events per second" >> messages/temp_message.txt

echo -e '\nsysbench --test=cpu --num-threads=8 --cpu-max-prime=20000 run' >> messages/temp_message.txt
cpu_events_per_second=$(sysbench --test=cpu --cpu-max-prime=20000 --num-threads=8 run | grep -oP '(?<=events per second: )(.*)(?=\.)')
echo "$cpu_events_per_second" > sh/cpu_events_per_second2.txt
echo "    $cpu_events_per_second events per second" >> messages/temp_message.txt

echo -e '\nsysbench --test=memory --num-threads=4 run' >> messages/temp_message.txt
result=$(sysbench --test=memory --num-threads=4 run | grep 'operations\|transferred')
mib_per_sec=$(echo $result | grep -oP '(?<=transferred \()(.*?)(?=M)')
echo "$mib_per_sec" > sh/mib_per_second.txt
echo "$result" >> messages/temp_message.txt

echo -e '\nsysbench --test=mutex --num-threads=64 run' >> messages/temp_message.txt
result=$(sysbench --test=mutex --num-threads=64 run | grep 'total time:')
mutex_time=$(echo $result | grep -oP '(?<=total time: )(.*?)(?=s)')
echo "$mutex_time" > sh/mutex_time.txt
echo " $result" >> messages/temp_message.txt

rm -R sh/testing/
rm -R sh/compare/

exit
